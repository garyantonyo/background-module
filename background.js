Module.register("background", {

    // Default module config.
    defaults: {
        updateInterval : 1000 * 60 * 60, // 1 hour
		directory: './'
    },

    start: function() {
        Log.info("Starting module: " + this.name);

        this.getBackground();
        this.scheduleUpdate();
    },
    // Define required styles.
    getStyles: function() {
        return ["background.css"];
    },

    getBackground: function() {
        this.sendSocketNotification("GET_BACKGROUND", {
			dirPath: this.config.directory
        });
    },

    socketNotificationReceived: function(notification, payload) {
        if (notification === "BACKGROUND_GOT") {
            Log.info(payload);
            var theBody = document.getElementsByTagName("BODY")[0];
			setTimeout(function(){
				theBody.style.backgroundImage = "url(./" + payload.background + ")";
			},1000)
			
        }
    },

    // Override dom generator.
    getDom: function() {
        var wrapper = document.createElement("div");
        return wrapper;
    },
    scheduleUpdate: function() {
        var self = this;
        setInterval(function() {
            self.getBackground();
        }, this.config.updateInterval);
    }

});
