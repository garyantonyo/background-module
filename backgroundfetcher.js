
var fs = require("fs");
var path = require('path')
var readline = require("readline");
var BackgroundFetcher = function(curDir) {
	var directory = curDir
	var self = this;
	var backgrounds = []
	
	/* startFetch()
	 * look for valid images in the directory (specified in the config)
	 */
	this.startFetch = function(){
		backgrounds = []
		directory = path.resolve(directory)
		var files = fs.readdirSync(directory)
		var validExtensions = [".jpeg",".jpg",".png"]
		for(file in files){
			for(extension in validExtensions){
				if(files[file].indexOf(validExtensions[extension]) !== -1){
					backgrounds.push(files[file])
				}
			}
		}
		this.broadcastEvents()
	}
	
	/* broadcastEvents()
	 * Broadcast the existing events.
	 */
	this.broadcastEvents = function() {
		eventsReceivedCallback(self);
	};
	
	/* rmDir()
	 * removes a directory
	 */
	var rmDir = function(imageDir){
		if(fs.existsSync(imageDir)){
			var files = fs.readdirSync(imageDir);
			if(files.length > 0){
				for(file in files){
					var filePath = imageDir + '/' + files[file];
					if(fs.statSync(filePath).isFile()){
						fs.unlinkSync(filePath);
					}
				}
			}
		}
	}
	
	/* getABackground()
	 * gets a background, copies it to a working directory
	 * then sends the path to that working copy to the client
	*/
	this.getABackground= function(){
		var imageDir = "./modules/background/images/"
		
		rmDir(imageDir);
		
		if (!fs.existsSync(imageDir)){
			fs.mkdirSync(imageDir);
		}
		
		//TODO this might not work on linux stuff
		var randIndex = Math.floor(Math.random()*backgrounds.length);
		
		var inPath = path.resolve(directory + "/" + backgrounds[randIndex])
		var outPath = path.resolve("./modules/background/images/" + backgrounds[randIndex])
		
		var inStr = fs.createReadStream(inPath);
		var outStr = fs.createWriteStream(outPath);

		inStr.pipe(outStr);
		
		return imageDir + backgrounds[randIndex];
	}
	
	/* onReceive(callback)
	 * Sets the on success callback
	 *
	 * argument callback function - The on success callback.
	 */
	this.onReceive = function(callback) {
		eventsReceivedCallback = callback;
	};

	/* onError(callback)
	 * Sets the on error callback
	 *
	 * argument callback function - The on error callback.
	 */
	this.onError = function(callback) {
		fetchFailedCallback = callback;
	};

}
module.exports = BackgroundFetcher;