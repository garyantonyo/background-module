# Background
A module for MagicMirror<sup>2</sup> that displays a set of random backgrounds from a directory.

## Dependencies
  * A [MagicMirror<sup>2</sup>](https://github.com/MichMich/MagicMirror) installation

## Installation
  1. Clone this repo into your `modules` directory.
  2. Create an entry in your `config.js` file to tell this module where to display on screen.
  
 **Example:**
```
 {
    module: 'background.js',
	position: 'top_left',
	config: {
		directory: 'D:/Desktop/wallpaper/That feel of total isolation and comfort',
		updateInterval : 1000 * 60 * 60
	}
 },
```

## Config
| **Option** | **Description** |
| --- | --- |
| `directory` | Set to directory that contains images |
| `updateInterval` | Set to desired update interval (in ms), default is `3600000` (10 hours). |
