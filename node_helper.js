var request = require('request');
var NodeHelper = require("node_helper");
var BackgroundFetcher = require("./BackgroundFetcher.js");

module.exports = NodeHelper.create({
	
	start: function() {
		var self = this;

		this.fetchers = [];

		console.log("Starting node helper for: " + this.name);
	},
	
	socketNotificationReceived: function(notification, payload) {
		var self = this;
		var label = payload.dirPath;
		
		if (notification === "GET_BACKGROUND") {
			
			fetcher = new BackgroundFetcher(payload.dirPath);
			
			fetcher.onReceive(function(fetcher) {
				self.sendSocketNotification("BACKGROUND_GOT", {
					background: fetcher.getABackground()
				});
			});
			
			fetcher.onError(function(fetcher, error) {
				self.sendSocketNotification("FETCH_ERROR", {
					error: error
				});
			});
			
			fetcher.startFetch();
		}
	},
});
